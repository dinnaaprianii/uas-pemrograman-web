<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/hello-laravel', function () {
        echo "ini halaman baru<br>";
        return "hello laravel";
    });

Route::get('/top-up-tagihan', function () {
        return "top up tagihan";
    });

Route::get('/payment', function () {
        return "ini menu payment";
    });