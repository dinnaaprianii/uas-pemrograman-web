<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->timestamps();
            $table->string('nm_karyawan');
            $table->date('tgl_lahir');
            $table->string('tempat_lahir', 80);
            $table->int('gaji_pokok');
            $table->bool('jenis_kelamin');
            $table->biginteger('nm_jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
