<!DOCTYPE html>
<html>
<head>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
.w3-row-padding img {margin-bottom: 12px}
/* Set the width of the sidebar to 120px */
.w3-sidebar {width: 120px;background: #222;}
/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */
#main {margin-left: 120px}
/* Remove margins from "page content" on small screens */
@media only screen and (max-width: 600px) {#main {margin-left: 0}}
</style>
</head>
<body class="w3-black">

<!-- Icon Bar (Sidebar - hidden on small screens) -->
<nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
  <!-- Avatar image in top left corner -->
  <img src="/assets/logo.png" style="width:50%">
  <a href="#" class="w3-bar-item w3-button w3-padding-large w3-black">
    <i class="fa fa-home w3-xxlarge"></i>
    <p>HOME</p>
  </a>
  <a href="#about" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-user w3-xxlarge"></i>
    <p>ABOUT</p>
  </a>
  <a href="#services" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-share-alt fa-3x"></i>
    <p>SERVICES</p>
  </a>
  <a href="#product" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-eye w3-xxlarge"></i>
    <p>PRODUCT</p>
  </a>
  <a href="#contact" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-envelope w3-xxlarge"></i>
    <p>CONTACT</p>
  </a>
</nav>

<!-- Navbar on small screens (Hidden on medium and large screens) -->
<div class="w3-top w3-hide-large w3-hide-medium" id="myNavbar">
  <div class="w3-bar w3-black w3-opacity w3-hover-opacity-off w3-center w3-small">
    <a href="#" class="w3-bar-item w3-button" style="width:25% !important">HOME</a>
    <a href="#about" class="w3-bar-item w3-button" style="width:25% !important">ABOUT US</a>
    <a href="#services" class="w3-bar-item w3-button" style="width:25% !important">SERVICES</a>
    <a href="#product" class="w3-bar-item w3-button" style="width:25% !important">PRODUCT</a>
    <a href="#contact" class="w3-bar-item w3-button" style="width:25% !important">CONTACT</a>
  </div>
</div>

<!-- Page Content -->
<div class="w3-padding-large" id="main">
  <!-- Header/Home -->
  <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
    <h1 class="w3-jumbo"><span class="w3-hide-small"></span>PT PRIMA DATA UTAMA SENTOSA</h1>
    <img src="/assets/hhh.jpg" width="950" height="600">
  </header>

  <!-- About Section -->
  <div class="w3-content w3-justify w3-text-grey w3-padding-64" id="about">
    <h2 class="w3-text-light-grey">ABOUT US</h2>
    <hr style="width:200px" class="w3-opacity">
    <p>PT Prima Data Utama Sentosa adalah perusahaan yang bergerak dibidang Consulting dan Teknologi Informasi khusunya pada pembuatan Sistem, Consulting,
      infrastruktur Jaringan dan supportnya, yaitu : Sistem Aplikasi, Consulting, Structured Cabling System, Network Infrastructured (LAN,WLAN,WAN)
      UPS System, Raised Floor dan Support & Maintenance.
          </p>
    <p class="w3-text-light-grey">Terpercaya & Handal</p>
    <div class="w3-white">
      <div class="w3-dark-grey">PT Prima Data Utama Sentosa didukung oleh professional yang berpengalaman dan ahli serta berbakat dan kreatif yang selalu berusaha
        untuk kesempurnaan dan menjaga kepuasan pelanggan. Kami percaya bahwa berjuang untuk kesempurnaan adalah kunci sukses, dan total solusi serta komitmen, akan menghasilkan yang terbaik, yang kami suguhkan kepada Customer.
      </p>
    </div>
    </div>
    <p class="w3-text-light-grey">Customer Oriented</p>
    <div class="w3-white">
      <div class="w3-dark-grey">Dalam memberikan kepuasan pelanggan adalah perhatian utama kami. Ini adalah tujuan utama kami dalam setiap tugas yang kami emban, dan untuk mencapai ini kami menyediakan service total selepas
        kontrak layanan penjualan.
    <p>
</DIV>
    </div>
    <p class="w3-text-light-grey">Mitra Dukungan</p>
    <div class="w3-white">
      <div class="w3-dark-grey">Kemitraan strategis dengan principal & vendor terpercaya handal memungkinkan kami untuk menyediakan solusi yang terbaik namun biaya efektif.
    <p></div>
</div>
    <div class="w3-row w3-center w3-padding-16 w3-section w3-light-grey">
      <div class="w3-quarter w3-section">
        <span class="w3-xlarge">11+</span><br>
        Partner
      </div>
      <div class="w3-quarter w3-section">
        <span class="w3-xlarge">55+</span><br>
        Projects Done
      </div>
      <div class="w3-quarter w3-section">
        <span class="w3-xlarge">89+</span><br>
        Happy Clients
      </div>
      <div class="w3-quarter w3-section">
        <span class="w3-xlarge">150+</span><br>
        Meetings
      </div>
    </div>
    
    <!-- Grid for pricing tables -->
    <h3 class="w3-padding-16 w3-text-light-grey" id="services">Services</h3>
    <div class="w3-row-padding">
      <div class=""> Untuk menambah nilai dalam memberikan homework doing services solusi terbaik ditengah kompetisi bisnis, kami menjalin kerjasan dengan beberapa produsen mulai dari Cisco, Avaya, Corning, Netviel,
        Vivotek,AMP, Panduit,Hikvision,Krone dan lain-lain. Untuk beberapa produk seperti reddit homework help : 
        <li>Router</li>
        <li>Switch</li>
        <li>Cabling System</li>
        Guna memberikan hasil yang maksimal dalam perusahaan anda.
      </div>
      <div class="w3-row-padding" style="margin:0 -16px">
      <div class="">
        <p>PT Prima Data Utama Sentosa, menyediakan jasa : 
          <li>Consulting untuk analisa data perusahaan, business intelligency dan consulting lainnya.</li>
          <li>Structure Cabling System DATA, VOICE CABLING SYSTEM, UTP dan STP</li>
          <li>Fiber Optic Cabling System</li>
          <li>UPS & Stabilizer System</li>
          <li>Raised Floor</li>
          <li>Industry Racking System</li>
          <li>Access Control System</li> 
          <li>CCTV</li>
          <li>Computer Networking Services</li>
        </p>
      </div>
</div>
    <!-- End Grid/Pricing tables -->
    </div>
    
    <!-- Testimonials -->
    <h3 class="w3-padding-24 w3-text-light-grey">Our Partner</h3>  
    <img src="/assets/belden.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:80px">
    <img src="/assets/amp.jpeg" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:80px">
    <img src="/assets/cisco.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:80px">
    <img src="/assets/mikrotik.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:80px">
    
  <!-- End About Section -->
  </div>
  
  <!-- Portfolio Section -->
  <div class="w3-padding-64 w3-content" id="product">
    <h2 class="w3-text-light-grey">Our Product</h2>
    <hr style="width:200px" class="w3-opacity">

    <!-- Grid for photos -->
    <div class="" style="margin:0 -16px">
      <div class="">
        <p>A. Consulting untuk Analis, BI, teknologi Informasi dan sebagainya</p>
        <p>PT Prima Data Utama Sentosa dapat memberikan solusi dari tahap awal menganalisa kebutuhan project sampai dengan melayani semua kebutuhan hingga tercapainya hasil 
          yang maksimal serta memuaskan, koordinasi, sosialisasi dan analisa yang matang adalah kunci kami dalam memanage sebuah project.
      </div>

      <div class="">
        <p>B. Implementasi Sistem</p>
        <p>Terdiri dari Router : Gateway, DNS Server, Mail Server, Proxy Server, Bandwitch Management, Firewall VPN, Virtualisasi, Wireless P2P,etc</p>
      </div>

      <div class="">
        <p>C. Software Development</p>
        <p>Kami menawarkan berbagai macam solusi untuk pembuatan aplikasi bersifat client server & web base.</p>
      </div>

      <div class="">
        <p>D. Jaringan & Data Center</p>
        <p>Sistem Kabel ( UTP & FO ), Wireless AP, Router & Switch, Server & Storage, UPS & Air Condition, Fire Suppression, Fire Alarm.
      </div>
    <!-- End photo grid -->
    </div>
  <!-- End Portfolio Section -->
  </div>

  <!-- Contact Section -->
  <div class="w3-padding-64 w3-content w3-text-grey" id="contact">
    <h2 class="w3-text-light-grey">Contact Me</h2>
    <hr style="width:200px" class="w3-opacity">

    <div class="w3-section">
      <p><i class="fa fa-map-marker fa-fw w3-text-white w3-xxlarge w3-margin-right"></i> Jakarta</p>
      <p><i class="fa fa-phone fa-fw w3-text-white w3-xxlarge w3-margin-right"></i> Phone: +62 8136669999</p>
      <p><i class="fa fa-envelope fa-fw w3-text-white w3-xxlarge w3-margin-right"> </i> Email: dinnaaprianii@gmail.com</p>
    </div><br>
    <p>Let's get in touch. Send me a message:</p>

    <form action="/action_page.php" target="_blank">
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Name" required name="Name"></p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Email" required name="Email"></p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Subject" required name="Subject"></p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Message" required name="Message"></p>
      <p>
      <a href="mailto:dinnaaprianii@gmail.com">Send Email</a><i class="fa fa-paper-plane"></i>    
      </p>
    </form>
  <!-- End Contact Section -->
  </div>
  
    <!-- Footer -->
  <footer class="w3-content w3-padding-64 w3-text-grey w3-xlarge">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
    <p class="w3-medium">Powered by <a href="https://primadata.net/" target="_blank" class="w3-hover-text-green">PT PRIMA DATA UTAMA SENTOSA</a></p>
  <!-- End footer -->
  </footer>

<!-- END PAGE CONTENT -->
</div>

</body>
</html>
